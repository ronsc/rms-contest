import { RmsContestPage } from './app.po';

describe('rms-contest App', () => {
  let page: RmsContestPage;

  beforeEach(() => {
    page = new RmsContestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
