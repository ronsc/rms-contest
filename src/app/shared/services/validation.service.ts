import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Injectable()
export class ValidationService {

  static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
    let config = {
      'required': 'กรุณาใส่ข้อมูล',
      'maxlength': `ใส่ข้อมูลสูงสุดไม่เกิน ${validatorValue.requiredLength} ตัวอักษร`,
      'maxDecimal': `ค่าสูงสุดไม่เกิน ${validatorValue}`,
      'rangeDecimal': `ข้อมูลต้องอยู่ในช่วงระหว่าง ${validatorValue.min} ถึง ${validatorValue.max}`,
    };

    return config[validatorName];
  }

  static maxDecimal(max: number) {
    return (control: AbstractControl) => {
      if (!max) return null;

      let v: number = +control.value;
      if (!v) return null;

      return v <= +max ? null : { maxDecimal: max };
    }
  }

  static rangeDecimal(min: number, max: number) {
    return (control: AbstractControl) => {
      if (!min || !max) return null;

      let v: number = +control.value;
      if (!v) return null;

      return (v >= +min && v <= +max) ? null : {
        rangeDecimal: { min, max }
      };
    }
  }

}
