import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { PackageType } from '../models/res-info';

@Injectable()
export class PackageTypeService {
  private _apiUrl = `${environment.api}/package-type`;

  constructor(private _http: Http) { }

  findAll(): Observable<PackageType[]> {
    return this._http.get(`${this._apiUrl}/findall`)
      .map(res => res.json() as PackageType[]);
  }

}
