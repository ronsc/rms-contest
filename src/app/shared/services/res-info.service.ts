import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { environment } from '../../../environments/environment';

import { ResInfo, TaxPaymentType, ServiceChargeType, PrintReceiptType, CurrencyType, PackageType } from '../models/res-info';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class ResInfoService {
  private _selected: ResInfo = new ResInfo();
  private _apiUrl = `${environment.api}/restaurant-info`;

  constructor(private _http: Http) { }

  findAll(): Observable<ResInfo[]> {
    return this._http.get(`${this._apiUrl}/findall`)
      .map(res => res.json() as ResInfo[])
      .catch(this.handleError);
  }

  findbyObject(data: any): Observable<ResInfo[]> {
    return this._http.post(`${this._apiUrl}/findbyobject`, data)
      .map(res => res.json() as ResInfo[])
      .catch(this.handleError);
  }

  add(model: ResInfo): Observable<ResInfo> {
    model = this.nullIdToNullObject(model);

    return this._http.post(`${this._apiUrl}/add`, model)
      .map(res => res.json() as ResInfo)
      .catch(this.handleError);
  }

  update(model: ResInfo): Observable<ResInfo> {
    model = this.nullIdToNullObject(model);

    return this._http.post(`${this._apiUrl}/update`, model)
      .map(res => res.json() as ResInfo)
      .catch(this.handleError);
  }

  remove(model: ResInfo): Observable<boolean> {
    delete model['$$index'];

    return this._http.post(`${this._apiUrl}/delete`, model)
      .map(() => true)
      .catch(this.handleError);
  }

  set selected(model: ResInfo) {
    this._selected = this.createSubObject(model);
  }

  get selected() {
    return this._selected;
  }

  private createSubObject(model: ResInfo): ResInfo {
    const obj = { ...model };
    obj.taxPaymentType = obj.taxPaymentType || new TaxPaymentType();
    obj.serviceChargeType = obj.serviceChargeType || new ServiceChargeType();
    obj.printReceiptType = obj.printReceiptType || new PrintReceiptType();
    obj.currencyType = obj.currencyType || new CurrencyType();
    obj.packageType = obj.packageType || new PackageType();

    return obj;
  }

  private nullIdToNullObject(model: ResInfo): ResInfo {
    const obj = { ...model };
    obj.taxPaymentType = obj.taxPaymentType.id ? obj.taxPaymentType : null;
    obj.serviceChargeType = obj.serviceChargeType.id ? obj.serviceChargeType : null;
    obj.printReceiptType = obj.printReceiptType.id ? obj.printReceiptType : null;
    obj.currencyType = obj.currencyType.id ? obj.currencyType : null;
    obj.packageType = obj.packageType.id ? obj.packageType : null;

    return obj;
  }

  private handleError(error: Response | any) {
    const body = error.json();
    let errMsg = 'เกิดข้อผิดพลาด: กรุณาติดต่อผู้ดูแลระบบ';

    if (error.status === 422) {
      errMsg = body ? body : errMsg;
    } else {
      errMsg = body.error;
    }

    console.error(error);

    return Observable.throw(errMsg);
  }

}
