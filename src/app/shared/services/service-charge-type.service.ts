import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ServiceChargeType } from '../models/res-info';
import 'rxjs/add/operator/map';

@Injectable()
export class ServiceChargeTypeService {
  private _apiUrl = `${environment.api}/service-charge-type`;

  constructor(private _http: Http) { }

  findAll(): Observable<ServiceChargeType[]> {
    return this._http.get(`${this._apiUrl}/findall`)
      .map(res => res.json() as ServiceChargeType[]);
  }

}
