import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PrintReceiptType } from '../models/res-info';
import 'rxjs/add/operator/map';

@Injectable()
export class PrintReceiptTypeService {
  private _apiUrl = `${environment.api}/print-receipt-type`;

  constructor(private _http: Http) { }

  findAll(): Observable<PrintReceiptType[]> {
    return this._http.get(`${this._apiUrl}/findall`)
      .map(res => res.json() as PrintReceiptType[]);
  }
}
