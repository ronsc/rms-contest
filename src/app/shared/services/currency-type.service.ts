import { CurrencyType } from '../models/res-info';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class CurrencyTypeService {
  private _apiUrl = `${environment.api}/currency-type`;

  constructor(private _http: Http) { }

  findAll(): Observable<CurrencyType[]> {
    return this._http.get(`${this._apiUrl}/findall`)
      .map(res => res.json() as CurrencyType[]);
  }

}
