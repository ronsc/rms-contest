import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TaxPaymentType } from '../models/res-info';
import 'rxjs/add/operator/map';

@Injectable()
export class TaxPaymentTypeService {
  private _apiUrl = `${environment.api}/tax-payment-type`;

  constructor(private _http: Http) { }

  findAll(): Observable<TaxPaymentType[]> {
    return this._http.get(`${this._apiUrl}/findall`)
      .map(res => res.json() as TaxPaymentType[]);
  }

}
