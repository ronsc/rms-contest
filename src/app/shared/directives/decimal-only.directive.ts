import { Directive, HostListener, ElementRef, Input } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Directive({
  selector: 'input[type="text"][decimalOnly]'
})
export class DecimalOnlyDirective {
  private allowedKeys: Array<string> = [
    'Enter', 'Backspace', 'Tab', 'Delete',
    'End', 'Home', 'ArrowLeft', 'ArrowRight',
  ];

  @Input() factor = 2;
  @Input() canMinus = false;

  constructor(private el: ElementRef) { }

  @HostListener('keydown', ['$event']) onKeyDown(event: KeyboardEvent) {
    if (this.canMinus) {
      this.allowedKeys = [...this.allowedKeys, '-'];
    }

    if (this.allowedKeys.indexOf(event.key) !== -1) {
      return;
    }

    const current: string = this.el.nativeElement.value;
    /**
     * How to get selection index.
     * https://stackoverflow.com/questions/5001608/how-to-know-if-the-text-in-a-textbox-is-selected
     */
    const position = event.target['selectionStart'];
    /**
     * How to concat string at index.
     * https://stackoverflow.com/questions/4364881/inserting-string-at-position-x-of-another-string
     */
    let next = '';
    if (current.charAt(0) === '-') {
      next = [current.slice(0, position + 1), event.key, current.slice(position + 1)].join('');
    } else {
      next = [current.slice(0, position), event.key, current.slice(position)].join('');
    }

    let decimalRegExStr = '^\\-?(\\d+)\\.?(\\d{1,' + this.factor + '})?$';

    if (new RegExp(decimalRegExStr).test(next)) {
      return;
    } else {
      event.preventDefault();
    }
  }

  /**
   * Onblur Event.
   * trim zero and dot from input value.
   */
  @HostListener('blur', ['$event']) onBlur(event: KeyboardEvent) {
    if (this.el.nativeElement.value) {
      this.el.nativeElement.value = parseFloat(this.el.nativeElement.value);
      this.el.nativeElement.value = isNaN(this.el.nativeElement.value) ? null : this.el.nativeElement.value;
    }
  }
}