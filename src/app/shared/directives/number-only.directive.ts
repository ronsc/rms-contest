import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: 'input[type="text"][numberOnly]'
})
export class NumberOnlyDirective {
  private regEx = new RegExp('^[0-9]*$');
  private allowedKeys: Array<string> = [
    'Enter', 'Backspace', 'Tab', 'Delete',
    'End', 'Home', 'ArrowLeft', 'ArrowRight'
  ];

  constructor(private el: ElementRef) { }

  @HostListener('keydown', ['$event']) onKeyDown(event: KeyboardEvent) {
    if (this.allowedKeys.indexOf(event.key) !== -1) {
      return;
    }

    if (this.regEx.test(event.key)) {
      return;
    } else {
      event.preventDefault();
    }
  }

  /**
   * Onblur Event.
   * trim zero from input value.
   */
  @HostListener('blur', ['$event']) onBlur(event: KeyboardEvent) {
    if (this.el.nativeElement.value) {
      this.el.nativeElement.value = parseFloat(this.el.nativeElement.value);
    }
  }
}