export class CommonType {
    id: number;
    name: string;
    active: string;
}

export class TaxPaymentType extends CommonType { }

export class ServiceChargeType extends CommonType { }

export class PrintReceiptType extends CommonType {
    description: string;
}

export class CurrencyType extends CommonType { }

export class PackageType extends CommonType {
    description: string;
}

export class ResInfo {
    id: number;
    code: string;
    name: string;
    address: string;
    tel: string;
    mobile: string;
    email: string;
    fax: string;
    website: string;
    taxIdNo: string;
    vatRate: number;
    serviceChargeRate: number;
    header: string;
    footer: string;
    latitude: number;
    longitude: number;
    active: string;
    startPackageDate: Date;
    taxPaymentType: TaxPaymentType;
    serviceChargeType: ServiceChargeType;
    printReceiptType: PrintReceiptType;
    currencyType: CurrencyType;
    packageType: PackageType;
}
