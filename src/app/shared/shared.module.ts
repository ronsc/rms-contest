import { CommonModule } from '@angular/common';
import { ControlMessagesComponent } from './components/control-messages.component';
import { CurrencyTypeService } from './services/currency-type.service';
import { DecimalOnlyDirective } from './directives/decimal-only.directive';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { NumberOnlyDirective } from './directives/number-only.directive';
import { PackageTypeService } from './services/package-type.service';
import { PrintReceiptTypeService } from './services/print-receipt-type.service';
import { ResInfoService } from './services/res-info.service';
import { ServiceChargeTypeService } from './services/service-charge-type.service';
import { TaxPaymentTypeService } from './services/tax-payment-type.service';
import { ValidationService } from './services/validation.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    DecimalOnlyDirective,
    NumberOnlyDirective,
    ControlMessagesComponent
  ],
  exports: [
    DecimalOnlyDirective,
    NumberOnlyDirective,
    ControlMessagesComponent
  ]
})
export class SharedModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        CurrencyTypeService,
        PackageTypeService,
        PrintReceiptTypeService,
        ServiceChargeTypeService,
        TaxPaymentTypeService,
        ResInfoService,
        ValidationService
      ]
    }
  }

}
