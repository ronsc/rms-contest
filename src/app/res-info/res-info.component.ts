import { Component, OnInit, ViewChild, TemplateRef, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ResInfoFormComponent } from './res-info-form/res-info-form.component';
import { ResInfoService } from '../shared/services/res-info.service';
import { ResInfo } from '../shared/models/res-info';

@Component({
  selector: 'app-res-info',
  templateUrl: './res-info.component.html',
  styleUrls: ['./res-info.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ResInfoComponent implements OnInit {
  rows = [];
  cols = [];
  formSearch: FormGroup;

  @ViewChild('selectTpl') selectTpl: TemplateRef<any>;
  @ViewChild('noTpl') noTpl: TemplateRef<any>;

  constructor(
    private _resInfoService: ResInfoService,
    private _modalService: NgbModal,
    private _fb: FormBuilder
  ) {
    this.createForm();
  }

  addResInfo() {
    this._resInfoService.selected = new ResInfo();

    this._modalService.open(ResInfoFormComponent, { windowClass: 'modal-lg-80' })
      .result.then(data => {
        this.rows.unshift(data);
      }, () => { });
  }

  editResInfo(item: ResInfo) {
    this._resInfoService.selected = item;
    this._modalService.open(ResInfoFormComponent, { windowClass: 'modal-lg-80' })
      .result.then(data => {
        this.rows = this.rows.map(row => {
          if (row.id === data.id) {
            return data;
          }
          return row;
        });
      }, () => { });
  }

  removeResInfo(item: ResInfo) {
    if (confirm("คุณแน่ใจที่จะลบข้อมูลหรือไม่")) {
      this._resInfoService.remove(item)
        .subscribe(() => {
          alert('ลบข้อมูลสำเร็จ');
          this.rows = this.rows.filter(row => row.id !== item.id);
        }, err => {
          alert(err);
        });
    }
  }

  search() {
    this._resInfoService.findbyObject(this.formSearch.value)
      .subscribe(data => {
        this.rows = data;
      },
      err => {
        alert(err);
      });
  }

  resetSearch() {
    this.formSearch.reset();

    this._resInfoService.findAll()
      .subscribe(data => this.rows = data);
  }

  ngOnInit() {
    this.cols = [
      { name: 'ลำดับ', prop: 'id', cellTemplate: this.noTpl, maxWidth: 100 },
      { name: 'รหัสร้าน', prop: 'code', maxWidth: 150 },
      { name: 'ชื่อร้าน', prop: 'name' },
      { name: 'ประเภทแพ็คเกจ', prop: 'packageType.name' },
      { name: '', cellTemplate: this.selectTpl, maxWidth: 200 },
    ];

    // Fetch restaurant-info.
    this._resInfoService.findAll()
      .subscribe(data => this.rows = data);
  }

  private createForm() {
    this.formSearch = this._fb.group({
      code: [''],
      name: ['']
    });
  }

}
