import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResInfoComponent } from './res-info.component';

const routes: Routes = [
  { path: 'restaurant-info', component: ResInfoComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResInfoRoutingModule { }
