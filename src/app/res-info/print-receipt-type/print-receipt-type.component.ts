import {
  Component,
  OnInit,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PrintReceiptTypeService } from '../../shared/services/print-receipt-type.service';

@Component({
  selector: 'app-print-receipt-type',
  templateUrl: './print-receipt-type.component.html',
  styles: []
})
export class PrintReceiptTypeComponent implements OnInit {
  rows = [];

  @ViewChild('selectTpl') selectTpl: TemplateRef<any>;

  constructor(private _printReceiptTypeService: PrintReceiptTypeService, public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this._printReceiptTypeService.findAll()
      .subscribe(data => this.rows = data);
  }

  get cols(): any[] {
    return [
      { name: 'ชื่อ', prop: 'name' },
      { name: 'รายละเอียด', prop: 'description' },
      { name: 'สถานะ', prop: 'active' },
      { name: '', cellTemplate: this.selectTpl }
    ];
  }

}
