import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { PackageTypeService } from '../../shared/services/package-type.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-package-type',
  templateUrl: './package-type.component.html',
  styles: []
})
export class PackageTypeComponent implements OnInit {
  rows = [];

  @ViewChild('selectTpl') selectTpl: TemplateRef<any>;

  constructor(private _packageTypeService: PackageTypeService, public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this._packageTypeService.findAll()
      .subscribe(data => this.rows = data);
  }

  get cols(): any[] {
    return [
      { name: 'ชื่อ', prop: 'name' },
      { name: 'รายละเอียด', prop: 'description' },
      { name: 'สถานะ', prop: 'active' },
      { name: '', cellTemplate: this.selectTpl }
    ];
  }

}
