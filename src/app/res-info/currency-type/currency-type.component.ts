import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { CurrencyTypeService } from '../../shared/services/currency-type.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-currency-type',
  templateUrl: './currency-type.component.html',
  styles: []
})
export class CurrencyTypeComponent implements OnInit {
  rows = [];

  @ViewChild('selectTpl') selectTpl: TemplateRef<any>;

  constructor(private _currencyTypeService: CurrencyTypeService, public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this._currencyTypeService.findAll()
      .subscribe(data => this.rows = data);
  }

  get cols(): any[] {
    return [
      { name: 'ชื่อ', prop: 'name' },
      { name: 'สถานะ', prop: 'active' },
      { name: '', cellTemplate: this.selectTpl }
    ];
  }
}
