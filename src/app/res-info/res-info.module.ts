import { CommonModule } from '@angular/common';
import { CurrencyTypeComponent } from './currency-type/currency-type.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PackageTypeComponent } from './package-type/package-type.component';
import { PrintReceiptTypeComponent } from './print-receipt-type/print-receipt-type.component';
import { ResInfoComponent } from './res-info.component';
import { ResInfoFormComponent } from './res-info-form/res-info-form.component';
import { ResInfoRoutingModule } from './res-info-routing.module';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ResInfoRoutingModule,
    // NgbModule,
    NgxDatatableModule,
    SharedModule
  ],
  declarations: [
    ResInfoComponent,
    ResInfoFormComponent,
    PrintReceiptTypeComponent,
    CurrencyTypeComponent,
    PackageTypeComponent
  ],
  entryComponents: [
    ResInfoFormComponent,
    PrintReceiptTypeComponent,
    CurrencyTypeComponent,
    PackageTypeComponent
  ],
})
export class ResInfoModule { }
