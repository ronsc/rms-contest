import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { NgbActiveModal, NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

import { ResInfoService } from '../../shared/services/res-info.service';
import { ResInfo, TaxPaymentType, ServiceChargeType, PrintReceiptType, CurrencyType, PackageType } from '../../shared/models/res-info';
import { TaxPaymentTypeService } from '../../shared/services/tax-payment-type.service';
import { PrintReceiptTypeService } from '../../shared/services/print-receipt-type.service';
import { ServiceChargeTypeService } from '../../shared/services/service-charge-type.service';
import { PrintReceiptTypeComponent } from '../print-receipt-type/print-receipt-type.component';
import { CurrencyTypeComponent } from '../currency-type/currency-type.component';
import { PackageTypeComponent } from '../package-type/package-type.component';
import { ValidationService } from '../../shared/services/validation.service';

@Component({
  selector: 'app-res-info-form',
  templateUrl: './res-info-form.component.html',
  styleUrls: ['./res-info-form.component.scss']
})
export class ResInfoFormComponent implements OnInit {
  form: FormGroup;
  resInfo: ResInfo = new ResInfo();
  taxPaymentTypes: TaxPaymentType[] = [];
  serviceChargeTypes: ServiceChargeType[] = [];

  private modalOptions: NgbModalOptions = {
    size: 'lg', backdrop: 'static'
  };

  constructor(
    private _fb: FormBuilder,
    private _resInfoService: ResInfoService,
    private _taxPaymentTypeService: TaxPaymentTypeService,
    private _serviceChargeTypeService: ServiceChargeTypeService,
    private _modalService: NgbModal,
    public activeModal: NgbActiveModal
  ) {
    this.resInfo = this._resInfoService.selected;

    this.createForm();
  }

  onSubmit() {
    this.resInfo = this.form.value;

    if (!this.resInfo.id) {
      this._resInfoService.add(this.resInfo)
        .subscribe(data => {
          alert('บันทึกสำเร็จ');
          // close modal
          this.activeModal.close(data);
        }, err => {
          alert(err);
        });
    } else {
      this._resInfoService.update(this.resInfo)
        .subscribe(data => {
          alert('แก้ไขสำเร็จ');
          // close modal
          this.activeModal.close(data);
        }, err => {
          alert(err);
        });
    }
  }

  addPrintReceiptType() {
    this._modalService.open(PrintReceiptTypeComponent, this.modalOptions)
      .result.then(data => {
        if (data) {
          this.resInfo.printReceiptType = data;
          this.form.patchValue({ printReceiptType: data });
        }
      }, () => { });
  }

  addCurrencyType() {
    this._modalService.open(CurrencyTypeComponent, this.modalOptions)
      .result.then(data => {
        if (data) {
          this.resInfo.currencyType = data;
          this.form.patchValue({ currencyType: data });
        }
      }, () => { });
  }

  addPackageType() {
    this._modalService.open(PackageTypeComponent, this.modalOptions)
      .result.then(data => {
        if (data) {
          this.resInfo.packageType = data;
          this.form.patchValue({ packageType: data });
        }
      }, () => { });
  }

  ngOnInit() {
    // Fetch Dropdown Data.
    this._taxPaymentTypeService.findAll()
      .subscribe(data => this.taxPaymentTypes = data);
    this._serviceChargeTypeService.findAll()
      .subscribe(data => this.serviceChargeTypes = data);

    // Set Form Value.
    this.form.patchValue({ ...this.resInfo });
  }

  private createForm() {
    this.form = this._fb.group({
      id: [],
      code: ['', [Validators.required, Validators.maxLength(10)]],
      name: ['', Validators.required],
      address: [''],
      tel: [''],
      mobile: [''],
      email: [''],
      fax: [''],
      website: [''],
      taxIdNo: ['', [Validators.maxLength(15)]],
      vatRate: ['', [Validators.required, ValidationService.maxDecimal(100)]],
      serviceChargeRate: ['', [Validators.required, ValidationService.maxDecimal(100)]],
      header: [''],
      footer: [''],
      latitude: ['', [ValidationService.rangeDecimal(-90, 90)]],
      longitude: ['', [ValidationService.rangeDecimal(-180, 180)]],
      startPackageDate: [null, Validators.required],
      taxPaymentType: this._fb.group({
        id: []
      }),
      serviceChargeType: this._fb.group({
        id: []
      }),
      printReceiptType: this._fb.group({
        id: [],
        name: []
      }),
      currencyType: this._fb.group({
        id: [],
        name: []
      }),
      packageType: this._fb.group({
        id: [],
        name: []
      }),
      active: ['1', Validators.required]
    });
  }

}
